#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"
#Set names for debugging. Comment it out if you put it as parameters (e.g. in Docker)
#epicsEnvSet("P", "PINK:")
#epicsEnvSet("R", "PICO1:")
#epicsEnvSet("DEVIP", "10.42.0.230")
#epicsEnvSet("DEVPORT", "5000")

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

drvAsynIPPortConfigure("PicoTermoPort","$(DEVIP):$(DEVPORT)",0,0,0)

dbLoadRecords("db/asynRecord.db","P=$(P)$(R),R=asyn,PORT=PicoTermoPort,ADDR=-1,OMAX=0,IMAX=0")

cd "${TOP}/iocBoot/${IOC}"
dbLoadRecords("PicoTerm.db","P=$(P), R=$(R)")

dbLoadRecords("PicoTerm.template","P=$(P), R=$(R), N=1")
dbLoadRecords("PicoTerm.template","P=$(P), R=$(R), N=2")
dbLoadRecords("PicoTerm.template","P=$(P), R=$(R), N=3")
dbLoadRecords("PicoTerm.template","P=$(P), R=$(R), N=4")
iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
