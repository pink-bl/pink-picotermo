record (ao, "$(P)$(R)TEMP$(N)")
{
    field (DESC, "Temperature channel 1")
    field (DTYP, "stream")
    field (OUT,  "@PicoTerm.proto Ch$(N)-T-get PicoTermoPort")
    field (EGU,  "C")
    field (PREC, "2")
    field (SCAN, "1 second")
}

record (ao, "$(P)$(R)TEMPIN$(N)")
{
    field (DESC, "Internal Ref Temperature")
    field (DTYP, "stream")
    field (OUT,  "@PicoTerm.proto IN$(N)-T-get PicoTermoPort")
    field (EGU,  "C")
    field (PREC, "2")
    field (SCAN, "1 second")
}

record (ao, "$(P)$(R)ERROR$(N)")
{
    field (DESC, "Chan ERROR output")
    field (DTYP, "stream")
    field (OUT,  "@PicoTerm.proto ERROR$(N)-get PicoTermoPort")
    field (PREC, "0")
    field (SCAN, "1 second")
}

record (calc, "$(P)$(R)EpicsERR$(N)")
{
    field (DESC, "Epics ERROR status")
    field (INPA, "$(P)$(R)TEMP$(N)")
    field (INPB, "$(P)$(R)TEMPIN$(N)")
    field (CALC, "(A&&B)=0?1:0")
    field (PREC, "0")
    field (SCAN, "1 second")
}

record (calc, "$(P)$(R)TEMP$(N)KC")
{
    field (DESC, "cels or kelv")
    field (INPA, "$(P)$(R)TEMP$(N)")
    field (INPB, "$(P)$(R)TEMPSwitch")
    field (CALC, "B=0?A:(A+273.15)")
    field (PREC, "2")
    field (EGU,  "deg")
    field (SCAN, "1 second")
}

record (calc, "$(P)$(R)SENS$(N)ERR1")
{
    field (DESC, "error bit 1")
    field (INPA, "$(P)$(R)ERROR$(N)")
    field (INPB, "$(P)$(R)ShiftBit")
    field (CALC, "(A-B)==1||(A-B)==3||(A-B)==5||(A-B)==7?1:0")
    field (PREC, "0")
    field (SCAN, "1 second")
}

record (calc, "$(P)$(R)SENS$(N)ERR2")
{
    field (DESC, "error bit 2")
    field (INPA, "$(P)$(R)ERROR$(N)")
    field (INPB, "$(P)$(R)ShiftBit")
    field (CALC, "(A-B)==2||(A-B)==3||(A-B)==6||(A-B)==7?1:0")
    field (PREC, "0")
    field (SCAN, "1 second")
}

record (calc, "$(P)$(R)SENS$(N)ERR3")
{
    field (DESC, "error bit 3")
    field (INPA, "$(P)$(R)ERROR$(N)")
    field (INPB, "$(P)$(R)ShiftBit")
    field (CALC, "(A-B)==4||(A-B)==5||(A-B)==6||(A-B)==7?1:0")
    field (PREC, "0")
    field (SCAN, "1 second")
}
